//
//  AppDelegate.h
//  Cert
//
//  Created by Digisa Digisa on 10/01/12.
//  Copyright (c) 2012 Digisa. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>{
    

    IBOutlet NSTextField * origemKey;
    IBOutlet NSTextField * destinoPem;
    IBOutlet NSTextField * nomeFinal;
    IBOutlet NSTextField * passWord;
    
}

@property (assign) IBOutlet NSWindow *window;

- (IBAction)TouchKey:(id)sender;

- (IBAction)TouchDestino:(id)sender;

- (IBAction)TouchGerarPEM:(id)sender;

@end
